# La calculatrice à pile

Dans le cadre du cours de programmation en Datascience, il nous a été demandé de réaliser un shell pour un langage de programmation très simple de notre invention. Le langage doit répondre au cahier des charges suivant:


•Utiliser la notation Polonaise tel PostScript, Forth ou RPL
•Le langage est implémenté au moyen d’un système de piles (deque).
•Le langage doit être utilisable interactivement au moyen du module cmd
•Implémenter les opérations arithmétiques élémentaires : add,rem,mul et div
•Implémenter les opérateurs de pile essentiels dup,drop,rot,swap,over
•Implémenter les opérateurs de pile composites -rot,nip et tuck
•Des instructions de debugging hors-langage comme clear et print


