import cmd
from collections import deque


class Interpreteur(object): #classe contenant les opérations que l'utilisateur va effectuer 

    pile=deque()
    def __init__(self): 
        
        self.dict_operation={'add' : self.add, 'mul' : self.mul, 'rem' : self.rem, 'tuck' : self.tuck,'nip' : self.nip,
                'over' : self.over, 'rot' : self.rot, 'dup' : self.dup, 'swap' : self.swap, 'div' : self.div, 
                'drop' : self.drop, 'minusrot' :self.minusrot
                }
    


#Definition de mes opérations sur la pile, avec message d'erreur si pas assez d'éléments dedans

    def add(self):  #Addition d'une pile, si pas assez d'éléments dans pile, je lui dis 

        if len(self.pile) <2:
            print("Tu as besoin de deux éléments dans ta pile !")
        else:
            self.pile[-2]=self.pile[-1]+self.pile[-2]
            self.pile.pop()     

    def mul(self): #multiplication d'une pile

        if len(self.pile) <2:
            print("Tu as besoin de deux éléments dans ta pile !")
        else:
            self.pile[-2]=self.pile[-1]*self.pile[-2]
            self.pile.pop()

    def div(self):  #division d'une pile

        if len(self.pile) <2:
            print("Tu as besoin de deux éléments dans ta pile !")
        else:
            self.pile[-2]=self.pile[-1]/self.pile[-2]
            self.pile.pop()

    def rem(self): #soustraction d'une pile 

        if len(self.pile) <2:
            print("Tu as besoin de deux éléments dans ta pile !")
        else:
            self.pile[-2]=self.pile[-1]-self.pile[-2]
            self.pile.pop()

    def dup(self): #Je duplique le dernier élément de la pile 

        if len(self.pile) <1:
            print("Tu as besoin d'un élément dans ta pile !")
        self.pile.append(self.pile[-1])

    def drop(self): #j'enlève le dernier élément de la pile

        self.pile.pop()

    def swap(self): #Je switch les deux derniers éléments de la pile

        if len(self.pile) <2:
            print("Tu as besoin de deux éléments dans ta pile !")
        else:
            self.pile.append(self.pile[-1])
            self.pile[-2]=self.pile[-3]
            self.pile[-3]=self.pile[-1]
            self.pile.pop()

    def rot(self): # Décale les éléments de la pile d'une place vers la droite
        self.pile.rotate(-1)

    def over(self): # Je mets l'avant dernier élément de la pile devant
        if len(self.pile) < 2:
            print("Tu as besoin de deux éléments dans ta pile !")
        else:
            self.pile.append(self.pile[-2])

    def minusrot(self): # Décale les éléments de la pile d'une place vers la gauche
        self.pile.rotate(1)

    def nip(self): # je swap, et je drop 

        if len(self.pile) < 2:
            print("Tu as besoin de deux éléments dans ta pile !")
        else:
            self.swap()
            self.drop() 

    def tuck(self): #je swap et over

        if len(self.pile) < 2:
            print("Tu as besoin de deux éléments dans ta pile !")
        else:
            self.swap()
            self.over()
        



class NewLanguage(cmd.Cmd):  #programmation de l'interface

    interpreteur=Interpreteur()
    pile=interpreteur.pile

    intro = 'Bienvenue dans le nouveau language informatique le ChillAndCode:\n\nTape help pour savoir les commandes\n' 

    prompt = 'Chill and start : '
    ruler = ">"
    doc_header = ("\nEn écrivant help et les mots qui suivent, tu auras des informations sur les commandes.\n")


    def do_push(self,arg):  #Push sert à appeler les differents operations mise dans notre Interpreteur

        # Obtenir une liste contenant les mots
        liste_mots = arg.split()
        for arg in liste_mots:   #Je lis tout ce que l'utilisateur écrit
            if arg.isdigit():
                self.pile.append(int(arg))

    
            else:
                interpreteur=Interpreteur()
                op= interpreteur.dict_operation[arg]
                op()

    
    def do_clear(self,arg): # j'enlève tous les éléments de la pile

        self.pile.clear()


    
    def do_exit(self,arg): #pour sortir du programme

        print("Merci d'avoir utilisé ChillAndCode")
        return True

    def do_operation(self,arg):

        interpreteur=Interpreteur()
        op= interpreteur.dict_operation
        print("\nVoici une liste reprenant les différentes opérations possibles:")
        print(list(op),"\n")

    def do_print(self,arg): #je lui affiche la pile 

        print(self.pile)
    

#Renseigne de l'utilité des instructions à l'utilisateur 


    def help_exit(self):

        print("\nPartir de ChillAndCode\n")

    def help_print(self):

        print("\nMontre la pile dans son état actuel\n")

    def help_clear(self):

        print("\nEnlève les élements de la pile\n") 

    def help_push(self):
        
        print("\nPermet de push des élements dans la pile, et d'effectuer des opérations dedans\n"
        '\npar exemple : push 1 2 3 add, donne : (1 5) dans la pile\n')

    def help_operation(self):
        print("\nAffiche les différentes opérations possibles !\n")


NewLanguage().cmdloop() 
    
